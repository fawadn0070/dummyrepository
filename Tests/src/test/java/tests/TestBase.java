package tests;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Optional;
import org.testng.annotations.Parameters;
import configurations.BrowserstackManager;
import configurations.DriverManager;
import configurations.GlobalVariables;
import configurations.SessionDataManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import utility.EmailManager;
import utility.ExcelReader;
import utility.Log;
import utility.ReportManager;

public class TestBase implements GlobalVariables{

	private static final Logger Logs = Logger.getLogger(TestBase.class.getName());

	@BeforeSuite
	public void configurations(ITestContext context) {
		DOMConfigurator.configure("log4j.xml");
		Logs.info("Setting up Test data Excel sheet");
		//		Directory directory = new Directory();
		//		directory.clearFolder(screenshotFolder);
		//		directory.clearFolder(htmlReportPath);
		ExcelReader.setExcelFile(testDataPath);
		ExcelReader.clearColumnData(testDataSheet, resultColumn, testDataPath);
	}

	@BeforeMethod
	@Parameters("browser")
	public void initialization(@Optional("Optional Parameter") String browser) {
		//		WebDriver driver = launchBrowser();
		//		DriverManager.getInstance().setDriver(driver);
	}

	@AfterMethod
	public void updateStatus(ITestResult result) throws Exception {
		String status = SKIP;
		if(result.getStatus() == ITestResult.FAILURE) {
			status = FAIL;								
		}
		if(result.getStatus() == ITestResult.SUCCESS) {
			status = PASS;
		}
		Logs.info("Closing all the browser.");
		String testCaseName = (String) SessionDataManager.getInstance().getSessionData("testCaseName");
		ReportManager.endTest();
		DriverManager.getInstance().getDriver().quit();
		Log.endTestCase(testCaseName+" "+status);
	}

	@AfterClass
	public void emailReport() {
		EmailManager.sendEmail();
	}

	protected static WebDriver launchBrowser(String browser) throws FileNotFoundException, IOException, ParseException {
		//		String browser = Config.getProperty("browser");
		WebDriver driver = null;
		if(browser.equalsIgnoreCase("CHROME") || browser.equalsIgnoreCase("SAFARI")) {
			// WebDriverManager.chromedriver().arch32().setup();
			// WebDriverManager.chromedriver().setup();
			// ChromeOptions option = new ChromeOptions();
			// option.addArguments("--disable-infobars");
			// option.addArguments("--headless");
			// driver = new ChromeDriver(option);
			driver = BrowserstackManager.getInstance().getBrowserStackDriver().browserStack_setup(browser);	
			// DesiredCapabilities cap = DesiredCapabilities.chrome();
			// cap.setPlatform(Platform.LINUX);
			// driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),cap);	
		}else if(browser.equalsIgnoreCase("EDGE")){
			// WebDriverManager.edgedriver().setup();
			// driver = new EdgeDriver();
			driver = BrowserstackManager.getInstance().getBrowserStackDriver().browserStack_setup(browser);
		}else if(browser.equalsIgnoreCase("FIREFOX")){
			// WebDriverManager.firefoxdriver().setup();
			// driver = new FirefoxDriver();
			driver = BrowserstackManager.getInstance().getBrowserStackDriver().browserStack_setup(browser);
		}else{
			driver = BrowserstackManager.getInstance().getBrowserStackDriver().browserStack_setup(browser);
			return driver;
		}

		//		driver.get(baseURL);
		driver.manage().window().maximize();
		driver.manage().timeouts().pageLoadTimeout(60 ,TimeUnit.SECONDS);
		//		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		return driver;
	}

}
