package tests;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import configurations.BrowserstackManager;
import configurations.DriverManager;
import configurations.ExcelManager;
import configurations.SessionDataManager;
import engine.KeyWordEngine;
import listners.TestListener;
import utility.BrowserstackDriver;
import utility.DateUtility;
import utility.ExcelReader;
import utility.Log;
import utility.ReportManager;
/**
 * 
 * @author FawadAhmad
 *
 */
//@Listeners(TestListener.class)
public class testRunner extends TestBase {
	
	public static List<String> steps = new LinkedList<String>();
	
	@Test(dataProvider = "testCasesList")
	public void testCasesExecutor(int testRow, String testCase, String testCategory, String testAuther, String browser) throws Exception{
		BrowserstackDriver brStack = new BrowserstackDriver();
		BrowserstackManager.getInstance().setBrowserStackDriver(brStack);
		WebDriver driver = launchBrowser(browser);
		DriverManager.getInstance().setDriver(driver);
		SessionDataManager.getInstance().setSessionData("testStartTime", DateUtility.getStringDate("hh.mm.ss aaa"));
		ExcelReader reader = new ExcelReader();
		ExcelManager.getInstance().setExcelReader(reader);
		KeyWordEngine keyWordEngine = new KeyWordEngine();
		SessionDataManager.getInstance().setSessionData("testCaseName", testCase);
		SessionDataManager.getInstance().setSessionData("testCaseRow", testRow);
		Log.startTestCase(testCase);
		Capabilities caps = ((RemoteWebDriver) DriverManager.getInstance().getDriver()).getCapabilities();
		if(browser.equalsIgnoreCase("Chrome") || browser.equalsIgnoreCase("Firefox") || 
				browser.equalsIgnoreCase("Edge") || browser.equalsIgnoreCase("Safari")) {
			String browserName = caps.getBrowserName().toUpperCase();
			String browserVersion = caps.getVersion();
			String osName = caps.getCapability("platformName").toString();
			ReportManager.startTest(testCase, testCategory, testAuther, osName+"_"+browserName +"_Version_"+ browserVersion);
		}else {
			String deviceName = caps.getCapability("device").toString();
			String osVersion = caps.getCapability("os_version").toString();
			String browserName = caps.getCapability("browserName").toString();
			ReportManager.startTest(testCase, testCategory, testAuther, deviceName +"_OS_"+osVersion+"_Browser_"+ browserName);
		}
		keyWordEngine.startExecution(testCase);
		ExcelManager.getInstance().getExcelReader().setCellData(PASS, testRow, resultColumn, testDataPath, testDataSheet);
	}
	
	@DataProvider (name = "testCasesList", parallel = true)
	public Object[][] getTestCaseList(){
		KeyWordEngine executor = new KeyWordEngine();
		Map<Integer, List<String>> mapOfTestCases = executor.getListOfTestCasesToExecute();
		Object[][] testCaseList = new Object[mapOfTestCases.size()][5];
		int i=0;
		for (Entry<Integer, List<String>> mapIterator: mapOfTestCases.entrySet()) {
			testCaseList[i][0] = mapIterator.getKey(); 
			testCaseList[i][1] = mapIterator.getValue().get(0);
			testCaseList[i][2] = mapIterator.getValue().get(1);
			testCaseList[i][3] = mapIterator.getValue().get(2);
			testCaseList[i][4] = mapIterator.getValue().get(3); 
			i++;
		}			
		return testCaseList;
	}
	
//	@Test
//	public void mytest() {
//		throw new SkipException("test is skipped");
//	}
}
