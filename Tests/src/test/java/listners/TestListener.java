package listners;

import java.util.Arrays;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;

import reporting.ExtentManager;
import tests.testRunner;

public class TestListener implements ITestListener {
	
	private static ExtentReports extent = ExtentManager.createInstance();
	private static ThreadLocal<ExtentTest> extentTest = new ThreadLocal<ExtentTest>();

	@Override
	public void onTestFailure(ITestResult result) {
		ITestListener.super.onTestFailure(result);
		
		String methodName = result.getMethod().getMethodName();
		String exceptionMessage = Arrays.toString(result.getThrowable().getStackTrace());
		extentTest.get().fail("<details><summary><b><font color=red>" + "Exception Occurred, click to see details:" +
				"</b></font></summary>" + exceptionMessage.replaceAll(",", "<br>") + "</details> \n");
		
		String path = ExtentManager.takeScreenshot(methodName);
		try {
			extentTest.get().fail("<b><font color=red>" + "Screenshot of failure" + "</font></b>", 
					MediaEntityBuilder.createScreenCaptureFromPath(path).build());
		} catch (Exception e) {
			extentTest.get().fail("Test Fail, cannot attach screenshot.");
		}
		
		String logText = "<b>Test Method " + result.getMethod().getMethodName() + " Failed</b>";
		Markup markup = MarkupHelper.createLabel(logText, ExtentColor.RED);
		extentTest.get().log(Status.FAIL, markup);
		
//		String myMessage = System.ge
//		extentTest.get().fail("<details><summary><b><font color=red>" + "All console messages, click to see details:" +
//				"</b></font></summary>" + myMessage.replaceAll(",", "<br>") + "</details> \n");
			
		
	}

	@Override
	public void onTestStart(ITestResult result) {
		ITestListener.super.onTestStart(result);
		
		ExtentTest test = extent.createTest(result.getTestClass().getName() + " :: " + result.getMethod().getMethodName());
		extentTest.set(test);
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		ITestListener.super.onTestSuccess(result);
		
		String logText = "<b>Test Method " + result.getMethod().getMethodName() + " successful</b>";
		Markup markup = MarkupHelper.createLabel(logText, ExtentColor.GREEN);
		extentTest.get().log(Status.PASS, markup);
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		ITestListener.super.onTestSkipped(result);
		
		String logText = "<b>Test Method " + result.getMethod().getMethodName() + " skipped</b>";
		Markup markup = MarkupHelper.createLabel(logText, ExtentColor.YELLOW);
		extentTest.get().log(Status.SKIP, markup);
	}
	
	@Override
	public void onFinish(ITestContext context) {
		ITestListener.super.onFinish(context);
		
		if(extent != null) {
			extent.flush();
		}
	}
	
	
	
}
