package reporting;

import java.io.File;
import java.util.Date;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

import baseClasses.Base;


public class ExtentManager extends Base{
	private static ExtentReports extent;

	public static ExtentReports createInstance() {
		String fileName = getReportName();
		String directory = System.getProperty("user.dir")+"\\Reports\\";
		new File(directory).mkdir();
		String path = directory + fileName;
		ExtentSparkReporter reporter = new ExtentSparkReporter(path);
		reporter.config().setEncoding("utf-8");
		reporter.config().setDocumentTitle("Nestosh Automation Report");
		reporter.config().setReportName("Automation Test Results");
		reporter.config().setTheme(Theme.DARK);

		extent = new ExtentReports();
		extent.setSystemInfo("Organization", "Nestosh");
		extent.attachReporter(reporter);

		return extent;

	}

	static String getReportName() {
		Date d = new Date();
		String fileName = "Nestosh Automation Report" + d.toString().replace(":", "-") + ".html";
		return fileName;
	}

	public static String takeScreenshot(String methodName) {

		String fileName = getScreenshotName(methodName);
		String directory = System.getProperty("user.dir") + "\\Screenshots\\";
		new File(directory).mkdir();
		String path = directory + fileName;

		try {
			File screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(screenshot, new File(path));
			System.out.println("************************************");
			System.out.println("Screenshot Stored at: " + path);
			System.out.println("************************************");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return path;
	}

	static String getScreenshotName(String methodName) {
		Date d = new Date();
		String fileName = methodName + d.toString().replace(":", "-") + ".png";
		return fileName;
	}
}
