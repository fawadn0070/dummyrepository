package com.browserstack;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.openqa.selenium.By;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import configurations.DriverManager;
import configurations.ExcelManager;
import configurations.SessionDataManager;
import engine.KeyWordEngine;
import utility.DateUtility;
import utility.ExcelReader;
import utility.Log;
import utility.ReportManager;

public class SingleTest extends BrowserStackTestNGTest {

	public static List<String> steps = new LinkedList<String>();
	public KeyWordEngine keyWordEngine;
	
	@Test(dataProvider = "testCasesList")
	public void testCasesExecutor(int testRow, String testCase, String testCategory, String testAuther, String browser) throws Exception{
//		WebDriver driver = launchBrowser(browser);
		DriverManager.getInstance().setDriver(driver);
		SessionDataManager.getInstance().setSessionData("testStartTime", DateUtility.getStringDate("hh.mm.ss aaa"));
		ExcelReader reader = new ExcelReader();
		ExcelManager.getInstance().setExcelReader(reader);
		SessionDataManager.getInstance().setSessionData("testCaseName", testCase);
		SessionDataManager.getInstance().setSessionData("testCaseRow", testRow);
		Log.startTestCase(testCase);
		String deviceName = capabilities.getCapability("device").toString();
		String osVersion = capabilities.getCapability("os_version").toString();
		String browserName = capabilities.getCapability("browserName").toString();
		ReportManager.startTest(testCase, testCategory, testAuther, deviceName +"_OS_"+osVersion+"_Browser_"+ browserName);
		keyWordEngine = new KeyWordEngine();
		keyWordEngine.startExecution(testCase);
	}
	
	@DataProvider (name = "testCasesList", parallel = true)
	public Object[][] getTestCaseList(){
		KeyWordEngine executor = new KeyWordEngine();
		Map<Integer, List<String>> mapOfTestCases = executor.getListOfTestCasesToExecute();
		Object[][] testCaseList = new Object[mapOfTestCases.size()][5];
		int i=0;
		for (Entry<Integer, List<String>> mapIterator: mapOfTestCases.entrySet()) {
			testCaseList[i][0] = mapIterator.getKey(); 
			testCaseList[i][1] = mapIterator.getValue().get(0);
			testCaseList[i][2] = mapIterator.getValue().get(1);
			testCaseList[i][3] = mapIterator.getValue().get(2);
			testCaseList[i][4] = mapIterator.getValue().get(3); 
			i++;
		}			
		return testCaseList;
	}
}
