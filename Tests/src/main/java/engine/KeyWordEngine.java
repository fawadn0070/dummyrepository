package engine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.Random;
import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import com.aventstack.extentreports.MediaEntityBuilder;
import configurations.Config;
import configurations.DriverManager;
import configurations.ExcelManager;
import configurations.GlobalVariables;
import configurations.SessionDataManager;
import utility.ExcelReader;
import utility.Helper;
import utility.ReportManager;
import utility.ScreenshotUtility;

/**
 * 
 * @author fawad
 *
 */
public class KeyWordEngine implements GlobalVariables {

	WebDriver driver;
	String prop;
	static Workbook book;
	static Sheet sheet;
	Helper help;
	//	Base base;
	WebElement element;
	static int randNo;
	private static final Logger Log = Logger.getLogger(KeyWordEngine.class.getName());
	private static final String IS_LIST_ELEMENT_YES = "YES";

	public final String SCENARIO_SHEET_PATH = ".\\src\\main\\java\\scenarios\\scenarios.xlsx";

	public void startExecution(String sheetName) throws Exception {


		//		FileInputStream file = null;
		//		try {
		//			file = new FileInputStream(SCENARIO_SHEET_PATH);
		//		} catch (FileNotFoundException e) {
		//			e.printStackTrace();
		//		}
		//
		//		try {
		//			book = WorkbookFactory.create(file);
		//		} catch (InvalidFormatException e) {
		//			e.printStackTrace();
		//		} catch (IOException e) {
		//			e.printStackTrace();
		//		}
		//
		//		sheet = book.getSheet(sheetName);
		//		System.out.println("Number of rows in this sheet are: "+sheet.getLastRowNum());
		randNo = randomNumber(9999);

		int k = 0;
		int numberOfRows = ExcelReader.getNumberOfRows(sheetName);
		for (int row = 1; row < numberOfRows; row++) {
			//			try {
			String stepDscp = ExcelManager.getInstance().getExcelReader().getCellData((row),(k + 0),sheetName);
			String locatorType = ExcelManager.getInstance().getExcelReader().getCellData((row),(k + 1),sheetName);
			String locatorValue = ExcelManager.getInstance().getExcelReader().getCellData((row),(k + 2),sheetName);
			String action = ExcelManager.getInstance().getExcelReader().getCellData((row),(k + 3),sheetName);
			String value = ExcelManager.getInstance().getExcelReader().getCellData((row ),(k + 4),sheetName);
			String isList = ExcelManager.getInstance().getExcelReader().getCellData((row),(k + 5),sheetName);
			String strlistIndex = ExcelManager.getInstance().getExcelReader().getCellData((row),(k + 6),sheetName);

			int listIndex = 0;
			if(!strlistIndex.equalsIgnoreCase("NA")) {
				try {
					listIndex = Integer.parseInt(strlistIndex);
				} catch (Exception e) {
					// TODO: handle exception
				}

			}

			if(action.equalsIgnoreCase("randomkeys")) {
				value = randNo+value;
			}
			Log.info("Executing step: "+stepDscp);

			Log.info("Keyword: "+action+" Value: "+ value);			
			try {
				switch (action) {
				case "open browser":
					//					base = new Base();
					//					prop = base.init_properties();

					//					if (value.isEmpty() || value.equals("NA")) {
					driver = DriverManager.getInstance().getDriver();
					//					} else {
					//						driver = base.init_driver(value);
					//					}
					help = new Helper(driver);
					break;

				case "enter url":
					if (value.isEmpty() || value.equals("NA")) {
						driver.get(Config.getProperty("url"));
					} else {
						driver.get(value);
					}
					break;

				case "quit":
					//					driver.quit();
					break;

				case "wait":
					Thread.sleep(Integer.parseInt(value) * 1000);
					break;

				case "executeTest":
					startExecution(value);
					break;
					
				case "auditPage":
					execCommand(value,stepDscp);
					break;

				case "switchto":
					if(locatorType.equalsIgnoreCase("NA")) {
						driver.switchTo().parentFrame();
						break;
					}
				default:
					break;
				}

				//////////////////////////////////////////////////////////////////////////////
				if(!locatorType.equalsIgnoreCase("NA")) {
					if(isList.equalsIgnoreCase(IS_LIST_ELEMENT_YES)) {
						List<WebElement> elements = help.waitVisibility2(help.getElementBy(locatorType,locatorValue), listIndex);
						Log.info("Number of elements in the lists are: "+elements.size());
						if(elements.size() - 1 < listIndex || elements.get(listIndex).getAttribute("class").contains("unselectable")) {
							int c=0;
							for(WebElement el : elements) {
								if(!el.getAttribute("class").contains("unselectable")) {
									if(el.getAttribute("class").contains("selected")) {
										Log.info("This element is already selected.");
										break;
									}
									help.performListAction(elements, action, value, c);
									break;
								}
								c++;
							}
						}else {
							help.performListAction(elements, action, value, listIndex);
						}
					}else {
						element = help.waitVisibility(help.getElementBy(locatorType,locatorValue));
						help.performAction(element, action, value);
					}
					//					locatorType = null;
					//				break;
				}

			} catch (Exception | Error e) {
				Log.error("Exception Occurred while executing the step:\n", e);					
				String imageFilePath = ScreenshotUtility.takeScreenShot(driver, sheetName);
				ReportManager.getTest().fail(stepDscp+"<br/>Keyword: "+action+" | Value: "+ value, MediaEntityBuilder.createScreenCaptureFromPath(imageFilePath).build());
				ReportManager.getTest().info(e);
				int testCaseRow = (Integer) SessionDataManager.getInstance().getSessionData("testCaseRow");
				ExcelManager.getInstance().getExcelReader().setCellData(FAIL, testCaseRow, resultColumn, testDataPath, testDataSheet);
				throw e;
			}
			if(stepDscp !=null && !stepDscp.isEmpty() && !action.equalsIgnoreCase("executeTest")) {
				if (action.equalsIgnoreCase("getText")) {
					ReportManager.getTest().pass(stepDscp+"<br/>Keyword: "+action+" | Value: "+ help.getText());
				}else {
					ReportManager.getTest().pass(stepDscp+"<br/>Keyword: "+action+" | Value: "+ value);
				}
			}

		}
	}

	public int randomNumber(int maxNumber) {
		Random rand = new Random();
		return rand.nextInt(maxNumber) + 1;
	}

	public Map<Integer, List<String>> getListOfTestCasesToExecute() {
		Log.info("Getting List of Test Cases to execute.");
		return new ExcelReader().getTestCasesToRun(testDataSheet, runModeColumn, testCaseColumn,testCategory,testAuther,testBrowser);
	}
	
	public static synchronized void execCommand(String url, String testName ) throws IOException{
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd-HHmmss");  
		LocalDateTime now = LocalDateTime.now();  
		System.out.println(dtf.format(now));  
		ProcessBuilder builder = new ProcessBuilder(
				"cmd.exe",
				"/c",
				"lighthouse "+ url  +" --port=9001 --output=html --output-path="+ lightHouse_report_path + testName.replace(" ", "_") +"_"+ dtf.format(now).replace("-","_") + ".html");
		builder.redirectErrorStream(true);
		Process p = builder.start();
		BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
		String line;
		while (true) {
			line = r.readLine();
			if (line == null) { break; }
			Log.info(line);
		}
	}

}
