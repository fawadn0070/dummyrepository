package utility;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import configurations.GlobalVariables;
import exceptions.InvalidLocatorException;

public class Helper implements GlobalVariables {
	WebDriverWait wait;
	Actions act;
	WebDriver driver;
	JavascriptExecutor js;
	WebElement iframe;
	private static final Logger Log = Logger.getLogger(Helper.class.getName());
	
	String elementText;

	public String getText() {
		return elementText;
	}


	public void setText(String s) {
		this.elementText = s;
	}


	public Helper(WebDriver driver) {
		this.driver = driver;
		wait = new WebDriverWait(driver, 30);
		js = (JavascriptExecutor) driver;
		act = new Actions(driver);
	}


	public WebElement waitVisibility(By loc){
		//		try {
		//			Thread.sleep(3000);
		//		} catch (InterruptedException e) {
		//			// TODO Auto-generated catch block
		//			e.printStackTrace();
		//		}
		WebElement el = wait.until(ExpectedConditions.visibilityOfElementLocated(loc));
		//highlightElement(el);
		return el;
	}

	public List<WebElement> waitVisibility2(By loc, int elm_number){
		//		checkPageIsReady();
		for (int i=0; i<=15; i++){ 
			try {
				Thread.sleep(1000);
			}catch (InterruptedException e) {} 
			//To check page ready state.
			if (wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(loc)).size() -1 >= elm_number){
				return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(loc));
			}   
		}
		return wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(loc));
	}

	public void do_movotoBy(By loc) {
		act.moveToElement(waitVisibility(loc));
		act.perform();
	}

	public void do_click(By loc) {
		waitVisibility(loc).click();
	}

	public void performAction(WebElement element, String action, String value) throws Exception{
		checkPageIsReady();
		if(!element.getAttribute("class").contains("js-iframe")) {
			highlightElement(element);
		}
		if (action.equalsIgnoreCase("sendkeys")) {
			element.clear();
			element.sendKeys(value);
		} else if (action.equalsIgnoreCase("click")) {
			//			wait.until(ExpectedConditions.elementToBeClickable(element));
			Log.info("Clicking "+element.getText()+" Web Element.........");
			js.executeScript("arguments[0].click();", element);
		} else if (action.equalsIgnoreCase("isDisplayed")) {
			assertTrue(isElementDisplayed(element), "element did not display");
		} else if (action.equalsIgnoreCase("getText")) {
			getElementText(element);
		}else if (action.equalsIgnoreCase("moveto")) {
			do_movoto(element);
		}else if (action.equalsIgnoreCase("select")) {
			setDropdown(element, DropDowns.VISIBLETEXT.toString(), value);
		}else if (action.equalsIgnoreCase("randomkeys")) {
			element.clear();
			element.sendKeys(value);
		}else if (action.equalsIgnoreCase("switchto")) {
			driver.switchTo().frame(element);
		}else if (action.equalsIgnoreCase("scroll")) {
			do_scroll(element);
		}

	}

	public void performListAction(List<WebElement> elements, String action, String value,int listIndex) throws InterruptedException {
		WebElement el = elements.get(listIndex);
		checkPageIsReady();
		if(!el.getAttribute("class").contains("js-iframe")) {
			highlightElement(el);
		}

		if (action.equalsIgnoreCase("sendkeys")) {
			el.clear();
			elements.get(listIndex).sendKeys(value);
		} else if (action.equalsIgnoreCase("click")) {
			Log.info("Clicking "+el.getText()+" Web Element.........");
			el.click();
		} else if (action.equalsIgnoreCase("isDisplayed")) {
			el.isDisplayed();
		} else if (action.equalsIgnoreCase("getText")) {
			getElementText(el);
		}else if (action.equalsIgnoreCase("moveto")) {
			do_movoto(el);
		}else if (action.equalsIgnoreCase("scroll")) {
			do_scroll(el);
		}else if (action.equalsIgnoreCase("switchto")) {
			System.out.println("switching frame!!!");
			driver.switchTo().frame(el);
		}
	}

	public void do_movoto(WebElement el) {
		act = new Actions(driver);
		act.moveToElement(el);
		act.perform();
	}

	public void do_scroll(WebElement el){
		String script = "window.scrollBy"+el.getLocation();
		System.out.println(script);
		js.executeScript(script);
	}

	public void checkPageIsReady() {

		//Initially bellow given if condition will check ready state of page.
		if (js.executeScript("return document.readyState").toString().equals("complete")){ 
			System.out.println("Page Is loaded.");
			return; 
		} 

		//This loop will rotate for 25 times to check If page Is ready after every 1 second.
		//You can replace your value with 25 If you wants to Increase or decrease wait time.
		for (int i=0; i<30; i++){ 
			try {
				Thread.sleep(1000);
			}catch (InterruptedException e) {} 
			//To check page ready state.
			if (js.executeScript("return document.readyState").toString().equals("complete")){ 
				break; 
			}   
		}
	}

	public void setDropdown(WebElement el, String type, String val) {
		Select select = new Select(el);
		switch (type) {
		case "index":
			if(Integer.parseInt(val)>=0 || Integer.parseInt(val)<select.getOptions().size()) {
				select.selectByIndex(Integer.parseInt(val));
				break;
			}else{
				System.out.println("Provided index is out of range.");
				break;
			}
		case "value":
			select.selectByValue(val);
		case "visibletext":
			for(WebElement elm : select.getOptions()) {
				if(elm.getText().equals(val)) {
					select.selectByVisibleText(val);
					return;
				}
			}
			System.out.println("Given dropdown value is not available.");
			break;
		default:
			System.out.println("Given type is not correct.");
			break;
		}		
	}

	public void highlightElement(WebElement el) {
		String orginalSytle = el.getAttribute("style");
		try {
			js.executeScript("arguments[0].setAttribute('style', 'background: yellow; border: 2px solid red;');", el);
			Thread.sleep(1000);
			String script = "arguments[0].setAttribute('style','"+orginalSytle+"');";
			js.executeScript(script, el);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public By getElementBy(String locator, String selector) throws InvalidLocatorException {
		By byLocator = null;
		switch (locator.toLowerCase()) {
		case "id": {
			byLocator = By.id(selector);
			break;
		}
		case "name": {
			byLocator = By.name(selector);
			break;
		}
		case "partiallink": {
			byLocator = By.partialLinkText(selector);
			break;
		}
		case "link": {
			byLocator = By.linkText(selector);
			break;
		}
		case "classname": {
			byLocator = By.className(selector);
			break;
		}
		case "tagname": {
			byLocator = By.tagName(selector);
			break;
		}
		case "css": {
			byLocator = By.cssSelector(selector);
			break;
		}
		case "xpath": {
			byLocator = By.xpath(selector);
			break;
		}
		default:{
			throw new InvalidLocatorException("Invalid Locator type "+locator);
		}
		}
		return byLocator;
	}

	protected boolean isElementDisplayed(WebElement element) {
		try {
			return element.isDisplayed();
		} catch (Exception e) {
			return false;
		}
	}
	
	protected String getElementText(WebElement element) {
		String text = element.getText().trim();
		Log.info("Message: "+text);
		setText(text);
		return text;
	}
	
	protected void validateSuccessMessage(WebElement element, String data) {	
		assertEquals(getElementText(element), data);
	}
	
}
