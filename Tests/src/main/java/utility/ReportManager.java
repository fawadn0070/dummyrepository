package utility;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.ViewName;

import configurations.GlobalVariables;

public class ReportManager implements GlobalVariables{
	private static ExtentReports extent;
	static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();
	public static String path;

	//Create an extent report instance
	private static ExtentReports createInstance() throws IOException {
		String fileName = getReportName();
		path = htmlReportPath+fileName;
//		ExtentPDFReporter pdf = new ExtentPDFReporter(path);
		ExtentSparkReporter htmlReporter = new ExtentSparkReporter(path).viewConfigurer().viewOrder().as(new ViewName[] 
				{ViewName.DASHBOARD,ViewName.TEST,ViewName.CATEGORY,ViewName.DEVICE,ViewName.EXCEPTION,ViewName.LOG}).apply();
		htmlReporter.loadXMLConfig(new File(extentConfigFilePath));
		extent = new ExtentReports();
		extent.setSystemInfo("User: ", System.getProperty("user.name"));	
//		extent.setSystemInfo("URL: ", baseURL);
//		extent.setSystemInfo("Browser: ", "Chrome");
		extent.setSystemInfo("OS: ", System.getProperty("os.name"));
		extent.setSystemInfo("Version: ", System.getProperty("os.version"));		
		extent.attachReporter(htmlReporter); 
		return extent;
	}

	private static ExtentReports getInstance() throws IOException {
		if (extent == null) {
			createInstance();
		}
		return extent;
	}

	public static synchronized ExtentTest getTest() {
		return (ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId()));
	}

	public static synchronized void endTest() throws IOException {
		getInstance().flush();

	}

	public static synchronized ExtentTest startTest(String testName, String category, String auther, String device) throws IOException {
		ExtentTest test = getInstance().createTest(testName).assignAuthor(auther).assignCategory(category).assignDevice(device);
		extentTestMap.put((int) (long) (Thread.currentThread().getId()), test);
		return test;
	}
	
	static synchronized String getReportName() {
		Date d = new Date();
		String fileName = "Nestosh Automation Report" + d.toString().replace(":", "-") + ".html";
		return fileName;
	}

}