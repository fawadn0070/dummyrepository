package configurations;

import java.util.HashMap;
import java.util.Map;

import utility.BrowserstackDriver;

public class BrowserstackManager {
	private static BrowserstackManager browserStackDriver;
	static Map<Integer, BrowserstackDriver> brStackDriverMap = new HashMap<Integer, BrowserstackDriver>();
	
	private BrowserstackManager() {
		
	}
	
	public static BrowserstackManager getInstance() {
		if(browserStackDriver == null) {
			browserStackDriver = new BrowserstackManager();
		}
		return browserStackDriver;
	}
	
	public synchronized void setBrowserStackDriver (BrowserstackDriver brDriver) {
		brStackDriverMap.put((int) (long) (Thread.currentThread().getId()), brDriver);
	}

	public synchronized BrowserstackDriver getBrowserStackDriver () {		
		return brStackDriverMap.get((int) (long) (Thread.currentThread().getId()));
	}
}
