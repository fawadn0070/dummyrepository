package configurations;

public interface GlobalVariables {

	String baseDirectory = System.getProperty("user.dir");
	String configPath = baseDirectory+"/config.properties";

	// Application URL
	String baseURL = "https://";

	// Path to test data sheet with test cases to run and test case details
	String testDataPath = baseDirectory+"/resources/data/scenarios.xlsx";
	String testDataSheet = "TestCases";
	int testCaseColumn = 0;
	int testCaseDescriptionColumn = 1;
	int runModeColumn = 2;
	int resultColumn = 6;
	int testCategory = 3;
	int testAuther = 4;
	int testBrowser = 5;

	// Columns in Test Case sheet
	int testStepsColumn = 0;
	int testStepDescriptionColumn = 1;
	int keywordColumn = 2;
	int dataColumn = 3;	

	String PASS = "PASS";
	String FAIL = "FAIL";
	String SKIP = "SKIP";

	// Wait times
	int implicitWaitTime = 10;
	long objectWaitTime = 30;

	// Screenshot folder and file details
	String screenshotFolder = baseDirectory+"/screenshots/";
	String fileFormat = ".png";
	
	// Report related details 
	String extentConfigFilePath = baseDirectory+"/extent-config.xml";
	String htmlReportPath = baseDirectory+"/Reports/";
	String htmlFileName = "index.html";
	String json_config_file_path = baseDirectory + "/mobile.conf.json";
	String lightHouse_report_path = baseDirectory +"/LightHouse/";
}
