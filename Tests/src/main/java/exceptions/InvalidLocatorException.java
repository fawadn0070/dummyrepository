package exceptions;

@SuppressWarnings("serial")
public class InvalidLocatorException extends Exception{

	public InvalidLocatorException(String msg) {
		super(msg);
	}
}
